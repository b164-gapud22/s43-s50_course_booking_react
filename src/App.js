import logo from './logo.svg';
import './App.css';

// this is not save in public folder, the file needs to  be imported to be usable
import cartoon from './bojji.jpg';


function App() {
  return (
  <div>
    <h1>Welcome to the course booking of batch 164</h1>
    <h5>This is our project in React</h5>
    <h6>Come Visit our website</h6> 
     {/*This image came from the /src folder*/}
    <img src={cartoon}  alt="image not found"/>
     <img src="/bojji.jpg" alt=""/>  
    <h3>This is My Favorite Cartoon Character</h3>
  </div>
  );
}

export default App;
